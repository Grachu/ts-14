#include "server.h"
#include "client.h"

void Server::main() {
	std::cout << "serwer";
	//Losowanie liczby
	srand(time(NULL));
	randNumb = rand() % 255;
	std::cout << "\nWylosowana liczba: " << randNumb << "\n";
	//Initialize winsock
	wsData;
	ver = MAKEWORD(2, 2);
	wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0) {
		std::cerr << "Cant start winsock! \n";
		return;
	}

	//Create a listening socket
	ListeningSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (ListeningSocket == INVALID_SOCKET) {
		std::cerr << "Cant create a socket! Quitting... \n";
		return;
	}

	//Bind the socket to an ip address and port
	serverHint.sin_addr.S_un.S_addr = ADDR_ANY;
	serverHint.sin_family = AF_INET;
	serverHint.sin_port = htons(53333);

	if (bind(ListeningSocket, (sockaddr*)&serverHint, sizeof(serverHint)) == SOCKET_ERROR) {
		std::cout << "Can't bind socket! Error: " << WSAGetLastError() << "\n";
	}
	ps[0].clientLength = sizeof(ps[0].clAddr);
	ZeroMemory(&ps[0].clAddr, ps[0].clientLength);


	ps[1].clientLength = sizeof(ps[1].clAddr);
	ZeroMemory(&ps[1].clAddr, ps[1].clientLength);

	listen(ListeningSocket, 2);
	std::cout << "\nWszystko ruszylo! Czekam na graczy\n";
	//Enter the loop for joining players
	while (i == 1 || i == 0) {
		//std::cout << "Petla no.1!\n";
		//Adding players
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, &ps[i].clAddr, &ps[i].clientLength);
		unboxPacket();
		//std::cout << "odebralem O " << O << " A " << A << "!\n";
		if (O == CON && A == JOIN_REQ)
		{
			//std::cout <<"O "<< O << " A " << A <<"!\n";
			
			if (i == 0 || i == 1)
			{
				addPlayers(ps[i]);
				if (i == 0) {
					nullPacket();
					O = CON;
					A = WAIT;
					I = ps[0].id;
					packMessage();
					std::cout << "\nSent info to client_id: " << ps[i].id << " to WAIT\n";
					do sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
					while (!waitForACK(ps[0].id));
					
				}
				//std::cout << "Iteracja i " << i << "\n";
				i++;
			}
			if (i > 1)
			{
				nullPacket();
				O = CON;
				A = FULL;
				packMessage();
				sendto(ListeningSocket, packet, packet_size, 0, &ps[i].clAddr, ps[i].clientLength);
			}
		}
	//Error when recieved bytes ain't correct
		if (bytesRecieved == SOCKET_ERROR)
		{
			std::cerr << "Error in recvfrom(): " << WSAGetLastError() << " Quitting... \n";
		}
	}
	//Sending continue message to clients
	std::cout << "Mam graczy! Wysy�am do pierwszego i drugiego gracza informacje, aby nie czekali\n";
	nullPacket();
	O = CON;
	A = CNT;
	I = ps[0].id;
	packMessage();
	do sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
	while (!waitForACK(ps[0].id));
	
	nullPacket();
	O = CON;
	A = CNT;
	I = ps[1].id;
	packMessage();
	do sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
	while (!waitForACK(ps[1].id));
	//Starting the game!!!
	guessGame();
	//Wait for clients do end game
	std::cout << "\nCzekam na graczy, az zakoncza rozgrywke\n";
	sockaddr_in c;
	int cL = sizeof(c);
	
	while (i != 0) {
		nullPacket();
		ZeroMemory(&c, cL);
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&c, &cL);
		unboxPacket();
		doCopy();
		sendACK(c);
		Backup();
		if (O == FIN && A == END)
		{
			if (I == ps[0].id) {
				nullPacket();
				O = FIN;
				A = END;
				packMessage();
				sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
				ps[0].id = 0;
				ps[0].clientLength = 0;
				i--;
			}
			else if (I == ps[1].id) {
				nullPacket();
				O = FIN;
				A = END;
				packMessage();
				sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
				ps[1].id = 0;
				ps[1].clientLength = 0;
				i--;
			}
		}
	}
	std::cout << "\nKoniec! Sprzatam po sobie...\n";
	//Close the socket
	closesocket(ListeningSocket);
	//Cleanup winsock
	WSACleanup();
}
void Server::TimeCheck() {
	using namespace std::literals::chrono_literals;
	//std::cout << "Wywolano watek czasowy... \n";
	beginTime = currTime();
	durTime = ((ps[0].id + ps[1].id) * 99) % 100 + 30;
	std::cout << "Time of the game " << durTime << "\n";
	remainTime = durTime;
	//Loop that works in the other thread until time's over
	while (remainTime>0) {
		if (beginTime % 10 == currTime() % 10)
		{
			std::cout << "Remaining time: " << remainTime << "s\n";

			nullPacket();
			O = TIM;
			A = TIM_LEFT;
			I = ps[0].id;
			L = remainTime;
			packMessage();
			sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
			//while (!waitForACK(ps[0].id));
			//std::cout << "wyslalem info o czasie do a0\n";

			nullPacket();
			O = TIM;
			A = TIM_LEFT;
			I = ps[1].id;
			L = remainTime;
			packMessage();
			sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
			//while (!waitForACK(ps[1].id));
			//std::cout << "wyslalem info o czasie do a1\n";
		}
		if (wygranko) break;
		std::this_thread::sleep_for(1s);
		remainTime -= 1;
	}
	Time = false;
}

void Server::guessGame() {
	std::cout << "\nGame started\n";
	std::thread tim = std::thread([this] {this->TimeCheck(); });
	sockaddr_in c;
	int cL = sizeof(c);
	while (Time)
	{
		nullPacket();
		ZeroMemory(&c, cL);
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&c, &cL);
		unboxPacket();
		doCopy();
		sendACK(c);
		Backup();
		if (O == NUM && A == SND_NUM) {
			//std::cout << "Tak dostalem liczbe\n";
			if (L == randNumb) {
				wygranko = true;

				if (I == ps[0].id)
				{
					nullPacket();
					O = RES;
					A = WIN;
					I = ps[0].id;
					L = randNumb;
					packMessage();
					sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
					//while (!waitForACK(ps[1].id));

					nullPacket();
					O = RES;
					A = E_WIN;
					I = ps[1].id;
					L = randNumb;
					packMessage();
					sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
					//while (!waitForACK(ps[0].id));

					break;
				}
				else if (I == ps[1].id)
				{
					nullPacket();
					O = RES;
					A = E_WIN;
					I = ps[0].id;
					L = randNumb;
					packMessage();
					sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
					//while (!waitForACK(ps[0].id));

					nullPacket();
					O = RES;
					A = WIN;
					I = ps[1].id;
					L = randNumb;
					packMessage();
					sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
					//while (!waitForACK(ps[1].id));
					break;
				}
				else
				{
					sendErr(c);
				}
			}
			else
			{
				if (I == ps[0].id) {
					//std::cout << "no nie, zla liczba\n";
					nullPacket();
					O = NUM;
					A = NOT;
					I = ps[0].id;
					packMessage();
					sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
					// (!waitForACK(ps[0].id));
				}
				if (I == ps[1].id) {
					//std::cout << "no nie, zla liczba\n";
					nullPacket();
					O = NUM;
					A = NOT;
					I = ps[1].id;
					packMessage();
					sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
					// (!waitForACK(ps[1].id));
				}
			}
		}
	}
	tim.join();
	//When time's up send to both players
	//1st player
	if (!wygranko) {
		nullPacket();
		O = TIM;
		A = TIM_UP;
		I = ps[0].id;
		L = randNumb;
		packMessage();
		sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
		//while (!waitForACK(ps[0].id));
		//2nd player
		nullPacket();
		O = TIM;
		A = TIM_UP;
		I = ps[1].id;
		L = randNumb;
		packMessage();
		sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
		//while (!waitForACK(ps[1].id));

		//Info that all lost - to 1st
		nullPacket();
		O = RES;
		A = LOS;
		I = ps[0].id;
		L = randNumb;
		packMessage();
		sendto(ListeningSocket, packet, packet_size, 0, &ps[0].clAddr, ps[0].clientLength);
		//while (!waitForACK(ps[0].id));

		//To 2nd
		nullPacket();
		O = RES;
		A = LOS;
		I = ps[1].id;
		L = randNumb;
		packMessage();
		sendto(ListeningSocket, packet, packet_size, 0, &ps[1].clAddr, ps[1].clientLength);
		//while (!waitForACK(ps[1].id));
	}
}

bool Server::waitForACK(int id) {
	std::cout << "\nWaiting for ack... ";
	ZeroMemory(&cli, cliLength);
	nullPacket();
	bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&cli, &cliLength);
	unboxPacket();
	std::string I_to_String = I.to_string();
	int id_in = bitToInteger(I_to_String);
	/*while (O != ACK && A != OK)
	{
	bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, &c.clAddr, &c.clientLength);
	unboxPacket();
	}
	Sleep(100);*/
	if (id != id_in && O != ACK && A != OK)
	{
		return 0;
	}
	else {
		std::cout << "got ack \n";
		return 1;
	}
}
long long Server::currTime() {
	auto time = std::chrono::duration_cast< std::chrono::seconds >(std::chrono::system_clock::now().time_since_epoch()).count();
	return time;
}
void Server::doCopy() {
	OC = O;
	AC = A;
	IC = I;
	LC = L;
}
void Server::Backup() {
	O = OC;
	A = AC;
	I = IC;
	L = LC;
}
void Server::sendACK(sockaddr_in c) {
	nullPacket();
	O = ACK;
	A = OK;
	packMessage();
	sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&c, sizeof(c));
}
void Server::sendErr(sockaddr_in c) {
	nullPacket();
	O = CON;
	A = ERR;
	packMessage();
	sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&c, sizeof(c));
}
void Server::addPlayers(client &c) {
	//Add the new connection do the list of connected client
	do {
		nullPacket();
		O = CON;
		A = SEND_ID;
		c.id = rand() % 255 + 1;
		if (ps[0].id == ps[1].id) {
			c.id = rand() % 255 + 1;
		}
		I = c.id;
		packMessage();
		sendto(ListeningSocket, packet, packet_size, 0, &c.clAddr, c.clientLength);
		std::cout << "\nSent to client " << i << " id " << ps[i].id << "\n";
	} while (!waitForACK(c.id));
}
void Server::unboxPacket() {
	std::string tmp;
	std::bitset<8> bset;

	for (int a = 0; a<4; a++)
	{
		bset = packet[a];
		tmp += bset.to_string();
	}

	O = bitToInteger(tmp.substr(0, 6));
	A = bitToInteger(tmp.substr(6, 4));
	I = bitToInteger(tmp.substr(10, 8));
	L = bitToInteger(tmp.substr(18, 8));
}
void Server::packMessage()
{
	std::string pom;
	//Operation
	pom = O.to_string();
	//Answer
	pom += A.to_string();
	//ID number
	pom += I.to_string();
	//Number
	pom += L.to_string();
	//Uzupelnienie
	pom += U.to_string();

	std::string B1 = pom.substr(0, 8);
	std::string B2 = pom.substr(8, 8);
	std::string B3 = pom.substr(16, 8);
	std::string B4 = pom.substr(24, 8);

	packet[0] = bitToInteger(B1);
	packet[1] = bitToInteger(B2);
	packet[2] = bitToInteger(B3);
	packet[3] = bitToInteger(B4);
}
int Server::bitToInteger(const std::string &str)
{
	int liczba = 0;
	for (int i = str.size() - 1, p = 1; i >= 0; i--, p *= 2)
	{
		if (str[i] == '1')
			liczba += p;
	}
	return liczba;
}
void Server::nullPacket()
{
	O = EMPTY;
	A = EMPTY;
	L = EMPTY;
	I = EMPTY;
}
void Server::PrintData()
{
	std::cout << O << std::endl;
	std::cout << A << std::endl;
	std::cout << I << std::endl;
	std::cout << L << std::endl;
}