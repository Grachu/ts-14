#include "client.h"
#include "server.h"


//using namespace std;
void Client::main() {
	std::cout << "Klient" << std::endl;
	
	//Initialize winsock
	wsData;
	ver = MAKEWORD(2, 2);
	wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0) {
		std::cerr << "Cant start winsock! \n";
		return;
	}

	//Create a listening socket
	ListeningSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (ListeningSocket == INVALID_SOCKET) {
		std::cerr << "Cant create a socket! Quitting... \n";
		return;
	}

	//Create hint structure for the server
	serverHint.sin_addr.S_un.S_addr = ADDR_ANY;
	serverHint.sin_family = AF_INET;
	serverHint.sin_port = htons(53333);
	inet_pton(AF_INET, "192.168.43.223", &serverHint.sin_addr);

	// Connecting to server
	std::cout << "Proba otrzymania id. \n";
	
	O = CON;
	A = JOIN_REQ;
	packMessage();
	std::cout << "Wyslanie prosby o przydzial id. \n";
	//std::cout << "Wysylam " << O << " A: " << A << "\n";
	sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, sin_size);

	// Receiving first Message
	bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
	unboxPacket();
	//std::cout << "Otrzymano pakiet: O = " << O << "; A = " << A << "; \n";
	nullPacket();

	sendACK(serverHint);

	std::cout << "Wysylam potwierdzenie przydzialu id \n";


	while (O != CON && A != SEND_ID) {
		O = CON;
		A = JOIN_REQ;
		packMessage();
		std::cout << "Wyslanie prosby o przydzial id. \n";
		//std::cout << "Wysylam " << O << " A: "<< A<<"\n";
		sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, sin_size);

		// Receiving first Message
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
		//std::cout << "Otrzymano pakiet: O = " << O << "; A = " << A << "; \n";
	}

		//"Rozpakowywanie ID"
	//std::cout << "Odpakowuje Id. \n";
	std::string I_to_String = I.to_string();
	id = bitToInteger(I_to_String);
	std::cout << "Otrzymano Id: " << id << "\n";
	
	while (true)
	{
		nullPacket();
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
		if (O == CON && A == CNT)
		{
			std::cout << "Otrzymano komunikat aby kontynuowac. Wysylam potwierdzenie. \n";
			sendACK(serverHint);
			break;
		}
		else if (O == CON && A == WAIT)
		{
			std::cout << "Otrzymano komunikat aby czekac. Wysylam potwierdzenie. \n";
			sendACK(serverHint);

		}
	} 

	// Starting Sending Thread
	std::thread worker = std::thread([this] {this->doSending(); });

	//*Server starts game* 
	std::cout << "Rozpoczeto gre! \n\n";
	
	while (O != RES) 
	{	
		
		// Server's response listening
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
		
			// if answer is wrong
		if (I == id && O == NUM && A == NOT) // Sending ACK
		{
			std::cout << "Wyslana odpowiedz jest bledna. \n";
			//sendACK(serverHint);
		} 
			// if Server sends remaining time
		else if (I == id && O == TIM && A == TIM_LEFT) // Remaining time + ACK
		{	
			std::string L_to_string = L.to_string();
			integerL = bitToInteger(L_to_string);
			std::cout << "Pozostaly czas: " << integerL <<" sekund." << std::endl;
		//	sendACK(serverHint);
		}
			// if Server sends result
		else if (I == id && O == RES)
		{
			if (A == WIN) 
			{
				int numb;
				std::string L_to_String = L.to_string();
				numb = bitToInteger(L_to_String);
				std::cout << "Udalo Ci sie wygrac! Szukana liczba to: " << numb << std::endl;
				//doCopy();
				//sendACK(serverHint);
				//Backup();
				break;
			}

			else if (A == E_WIN) 
			{
				std::cout << "Niestety wygral twoj przeciwnik" << std::endl;
				//doCopy();
				//sendACK(serverHint);
				//Backup();
				break;
			}
			else if (A == LOS) 
			{
				std::cout << "Czas sie skonczyl! Nikt nie wygral." << std::endl;
				//doCopy();
				//sendACK(serverHint);
				//Backup();
				break;
			}
		} 
			// if time's up
		if (I == id && O == TIM && A == TIM_UP)
		{
			std::cout << "Czas sie skonczyl! Nikt nie wygral." << std::endl;
			//doCopy();
			//nullPacket();
			//O = ACK;
			//A = OK;
			//packMessage();
			//sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, sin_size);
			//Backup();
			break;
		}
	}

	// Przerwanie w�tku
	is_finished = true;
	
	std::cout << "Wprowadz dowolna liczbe aby przejsc dalej...\n";
	//std::cout << "\n thread id: " << worker.get_id() << " \n";
	worker.join();
	//std::cout << "Przerwano watek wysylajacy \n";
	// End of the game, disconnecting
	nullPacket();
	O = FIN;
	A = END;
	packMessage();
	
	std::cout << "Wysylanie info o rozlaczeniu. \n";
	sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, sin_size);	
	std::cout << "Otrzymano potwierdzenie od serwera \n";
	nullPacket();

	// Receiving "ACK"
	bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
	unboxPacket();
	if (I == id && O == FIN && A == END)
	{
		std::cout << "Serwer odebral info o rozlaczeniu." << std::endl;
	}

	// Finally
	std::cout << "Aby zakonczyc wcisnij klawisz enter." << std::endl;
	std::cin.get();
	
	// Close the socket
	closesocket(ListeningSocket);
	// Cleanup Winsock
	WSACleanup();
}


void Client::nullPacket()
{
	O = EMPTY;
	A = EMPTY;
	L = EMPTY;
}
void Client::unboxPacket() {
	std::string tmp;
	std::bitset<8> bset;

	for (int a = 0; a<4; a++)
	{
		bset = packet[a];
		tmp += bset.to_string();
	}

	O = bitToInteger(tmp.substr(0, 6));
	A = bitToInteger(tmp.substr(6, 4));
	I = bitToInteger(tmp.substr(10, 8));
	L = bitToInteger(tmp.substr(18, 8));
}
void Client::packMessage()
{
	std::string pom;
	//Operation
	pom = O.to_string();
	//Answer
	pom += A.to_string();
	//ID number
	pom += I.to_string();
	//Number
	pom += L.to_string();
	//Uzupelnienie
	pom += U.to_string();

	std::string B1 = pom.substr(0, 8);
	std::string B2 = pom.substr(8, 8);
	std::string B3 = pom.substr(16, 8);
	std::string B4 = pom.substr(24, 8);

	packet[0] = bitToInteger(B1);
	packet[1] = bitToInteger(B2);
	packet[2] = bitToInteger(B3);
	packet[3] = bitToInteger(B4);
}
int Client::bitToInteger(const std::string &str)
{
	int liczba = 0;
	for (int i = str.size() - 1, p = 1; i >= 0; i--, p *= 2)
	{
		if (str[i] == '1')
			liczba += p;
	}
	return liczba;
}
void Client::PrintData()
{
	std::cout << O << std::endl;
	std::cout << A << std::endl;
	std::cout << I << std::endl;
	std::cout << L << std::endl;
}
void Client::doCopy() {
	OC = O;
	AC = A;
	IC = I;
	LC = L;
}
void Client::Backup() {
	O = OC;
	A = AC;
	I = IC;
	L = LC;
}
bool Client::waitForACK() {
	nullPacket();
	int wait = 0;
	while (O != ACK && A != OK) {
		bytesRecieved = recvfrom(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, &sin_size);
		unboxPacket();
		wait++;
		if (wait == 100) return 0;
	}
	return 1;
}
void Client::sendACK(sockaddr_in serv) {
	nullPacket();
	O = ACK;
	A = OK;
	packMessage();
	sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serv, sizeof(serv));
	//std::cout << "\nwyslano ack!!!\n";
}
void Client::doSending() 
{	
	using namespace std::literals::chrono_literals;
	std::cout << "Rozpoczeto watek wysylajacy!" << std::endl;
	int answer;
	while (!is_finished) {
		std::cin >> answer;
		// Sending answer
		nullPacket();
		O = NUM;
		A = SND_NUM;
		L = answer;
		packMessage();
		sendto(ListeningSocket, packet, packet_size, 0, (sockaddr*)&serverHint, sin_size);
		//std::cout << "\nsent "<< answer <<"\n";
		nullPacket();
		std::this_thread::sleep_for(0.5s);
	}
	if (is_finished) {
		//std::cout << "\nXd\n";
		return;
	}
}