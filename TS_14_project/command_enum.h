#pragma once

enum MAIN { EMPTY };
enum O { CON, TIM, NUM, RES, ACK, FIN};				//Operations
enum AC { SEND_ID, JOIN_REQ, WAIT, CNT, FULL, ERR };//Answer CON
enum AT { TIM_LEFT, TIM_UP };						//Answer TIM
enum AN { SND_NUM, NOT };							//Answer NUM
enum AR { WIN, LOS, E_WIN};							//Answer RES
enum AA { OK };										//Answer ACK
enum AF { END };									//Answer FIN