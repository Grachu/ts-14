#pragma once
#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <thread>
#include <bitset>
#include <string>
#pragma comment(lib, "Ws2_32.lib")
#include <random>
#include <thread>
#include "command_enum.h"

class Client {
public:
	//Things connected with socket
	WSADATA wsData;
	sockaddr_in serverHint;
	WORD ver;
	int wsOk;
	SOCKET ListeningSocket;
	int sin_size = sizeof(sockaddr_in);
	int bytesRecieved;
	int id;

	//Packet tings
	const int packet_size = 4;
	char packet[4];

	//Thread tings
	bool is_finished = false;

	std::bitset<6> O;
	std::bitset<4> A;
	std::bitset<8> I = 0;
	std::bitset<8> L;
	std::bitset<6> U;
	int integerL;

	std::bitset<6> OC;
	std::bitset<4> AC;
	std::bitset<8> IC = 0;
	std::bitset<8> LC;

	
	void doSending();
	void nullPacket();
	void unboxPacket();
	void packMessage();
	int bitToInteger(const std::string&str);
	void PrintData();
	void doCopy();
	void Backup();
	bool waitForACK();
	void sendACK(sockaddr_in serv);
	void main();
};