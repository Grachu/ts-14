#pragma once
#include <iostream>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <thread>
#include <bitset>
#include <string>
#include <ctime>
#include <array>
#include <chrono>
#pragma comment(lib, "Ws2_32.lib")
#include <random>
#include "command_enum.h"

class Server {
public:
	//Things connected with sockets
	WSADATA wsData;
	sockaddr_in serverHint, cli;
	WORD ver;
	int wsOk;
	SOCKET ListeningSocket, AcceptSocket;
	int sin_size = sizeof(sockaddr_in);
	int bytesRecieved;
	int cliLength = sizeof(cli);

	struct client {
		int id;
		SOCKADDR clAddr;
		int clientLength;
	};

	//Program
	int randNumb;
	std::array<client, 3> ps;
	int i = 0;
	bool wygranko=false;
	// Time stuff
	long long beginTime, durTime, remainTime;
	long long currTime();
	bool Time = true;

	//Packet things
	const int packet_size = 4;
	char packet[4];

	std::bitset<6> O;
	std::bitset<4> A;
	std::bitset<8> I = 0;
	std::bitset<8> L;
	std::bitset<6> U;

	std::bitset<6> OC;
	std::bitset<4> AC;
	std::bitset<8> IC = 0;
	std::bitset<8> LC;

	void TimeCheck();
	void guessGame();
	void doCopy();
	void Backup();
	void addPlayers(client &c);
	bool waitForACK(int id);
	void sendACK(sockaddr_in c);
	void sendErr(sockaddr_in s);
	void nullPacket();
	void unboxPacket();
	void packMessage();
	void PrintData();
	int bitToInteger(const std::string&str);
	void deletePlayer(int n);
	void main();
};
